- name: Configure Odoo and PostgreSQL containers with Docker and back up volumes to Azure Blob Storage
  hosts: all
  become: true
  
  vars:
    postgres_password: "{{ lookup('env','POSTGRES_PASSWORD') }}"
    storage_account_name: "{{ 'dockerbackup' + lookup('pipe', 'date +%s') }}"
    container_name: "dockerbackups"
    azure_storage_connection_string: "VotreChaineDeConnexionAzureStorage" # Remplacez ceci par votre vraie chaîne de connexion
    backup_script_path: "/tmp/backup_docker_volumes.sh"

  tasks:
    - name: Add user 'azureuser' to the docker group
      user:
        name: azureuser
        groups: docker
        append: yes

    - name: Ensure 'azureuser' can run sudo without password
      lineinfile:
        path: /etc/sudoers.d/azureuser
        line: 'azureuser ALL=(ALL) NOPASSWD:ALL'
        create: yes

    - name: Pull Odoo Docker image
      docker_image:
        name: odoo:latest
        source: pull

    - name: Pull PostgreSQL Docker image
      docker_image:
        name: postgres:latest
        source: pull

    - name: Create network for Odoo and PostgreSQL containers
      docker_network:
        name: odoo_network

    - name: Create volume for Odoo
      docker_volume:
        name: odoo_data

    - name: Create volume for PostgreSQL
      docker_volume:
        name: postgres_data

    - name: Start Odoo container
      docker_container:
        name: odoo
        image: odoo:latest
        networks:
          - name: odoo_network
        ports:
          - "80:8069"
        volumes:
          - "odoo_data:/var/lib/odoo"
        restart_policy: unless-stopped

    - name: Start PostgreSQL container
      docker_container:
        name: postgres
        image: postgres:latest
        env:
          POSTGRES_USER: odoo
          POSTGRES_PASSWORD: "{{ postgres_password }}"
          POSTGRES_DB: postgres
        networks:
          - name: odoo_network
        ports:
          - "5432:5432"
        volumes:
          - "postgres_data:/var/lib/postgresql/data"
        restart_policy: unless-stopped

    - name: Création du script de sauvegarde Docker volumes
      copy:
        dest: "{{ backup_script_path }}"
        mode: '0755'
        content: |
          #!/bin/bash
          timestamp=$(date +%Y%m%d-%H%M%S)
          backup_dir="/var/backups"
          mkdir -p "${backup_dir}"
          odoo_backup_filename="odoo_data_${timestamp}.tar.gz"
          postgres_backup_filename="postgres_data_${timestamp}.tar.gz"

          # Sauvegarde des volumes Docker
          docker run --rm -v odoo_data:/data -v ${backup_dir}:/backup ubuntu tar czvf /backup/${odoo_backup_filename} /data
          docker run --rm -v postgres_data:/data -v ${backup_dir}:/backup ubuntu tar czvf /backup/${postgres_backup_filename} /data

          # Téléchargement des sauvegardes vers Azure Blob
          azcopy login --connection-string '{{ azure_storage_connection_string }}'
          azcopy copy "/var/backups/${odoo_backup_filename}" "https://{{ storage_account_name }}.blob.core.windows.net/{{ container_name }}/${odoo_backup_filename}"
          azcopy copy "/var/backups/${postgres_backup_filename}" "https://{{ storage_account_name }}.blob.core.windows.net/{{ container_name }}/${postgres_backup_filename}"

    - name: Configurer une tâche cron pour exécuter le script de sauvegarde chaque jour
      cron:
        name: "Sauvegarde quotidienne Docker volumes"
        minute: "59"
        hour: "23"
        job: "{{ backup_script_path }}"
        state: present
